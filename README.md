# Assignment 4
## description
Take your completed Exercise 4 App and expand it by adding the following:

Combines my exercise 4 code with the "Creating an audio player" recipe from "React Native Cookbook" to make a playlist with 3 songs.

## requirements

- Create a playlist with 3 songs/bites 
- Each song/sound bite must use a direct link to the MP3
- Replicate the interface for displaying the currently playing sound
  and moving between tracks
- Position the playlist code above your original exercise code
- Change your app to only display one of the three pickers for each sound.
- Only display the Picker for the song that is currently playing
- Allow the User to "rate" each sound with its corresponding picker.
- The picker values must still be saved and loaded as before.
- Record a video of your app running in either the Android Emulator or the iOS Simulator.
- Your video must show you cycling through all three sounds.
- It must also show you saving and loading at least one value.
- Put your video in the folder with your App, and zip the entire folder.
- Submit the zip folder in Canvas.

## authors
- Gregory Stocker
- Dan Ward- author of "React Native Cookbook"

## dependency commands
npm install @react-native-picker/picker --save
npx expo install expo-av