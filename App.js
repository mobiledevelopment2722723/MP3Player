import React, { Component } from 'react';
import { Audio } from 'expo-av';
import { Feather } from '@expo/vector-icons';


import {
  Alert,
  AsyncStorage,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  ScrollView,
  SafeAreaView
} from 'react-native';
import { Picker } from '@react-native-picker/picker';
import { createStackNavigator } from '@react-navigation/stack';
import 'react-native-gesture-handler';

//used to set the mane we will use to save the content
const key = '@MyApp:key';

//playlist holds the audio tracks, each track is represented by an object
//each object has title,artist, album and uri
const playlist = [
  {
    title: 'People Watching',
    artist: 'Keller Williams',
    album: 'Keller Williams Live at The Westcott Theater on 2012-09-22',
    uri: 'https://ia800308.us.archive.org/7/items/kwilliams2012-09-22.at853.flac16/kwilliams2012-09-22at853.t16.mp3',
  },
  {
    title: 'Hunted By A Freak',
    artist: 'Mogwai',
    album: 'Mogwai Live at Ancienne Belgique on 2017-10-20',
    uri: 'https://ia601509.us.archive.org/17/items/mogwai2017-10-20.brussels.fm/Mogwai2017-10-20Brussels-07.mp3',
  },
  {
    title: 'Nervous Tic Motion of the Head to the Left',
    artist: 'Andrew Bird',
    album: 'Andrew Bird Live at Rio Theater on 2011-01-28',
    uri: 'https://ia800503.us.archive.org/8/items/andrewbird2011-01-28.early.dr7.flac16/andrewbird2011-01-28.early.t07.mp3',

  }
];

export default class App extends Component {

  //the App's initial state
  state = {
    isPlaying: false, //boolean if the song is playing
    playbackInstance: null, //holds the Audio instance
    volume: 1.0, //for current track
    currentTrackIndex: 0,
    isBuffering: false, //to display a msg when the track buffers at the start
    ratings: ["I Don't Know", "I Don't Know", "I Don't Know"],
    questions: [
      `Rate the song ${playlist[0].title} `,
      `Rate the song ${playlist[1].title} `,
      `Rate the song ${playlist[2].title} `
    ],
    storedRatings: ["I Don't Know", "I Don't Know", "I Don't Know"]//this variable is used to restore from the local storage
  }
  /**
   * allowsRecordingIOS: false,
      playThroughEarpieceAndroid: true,
      interruptionModeIOS: INTERRUPTION_MODE_IOS_DO_NOT_MIX = 1,
      playsInSilentModeIOS: false,
      shouldDuckAndroid: true,
      interruptionModeAndroid: INTERRUPTION_MODE_ANDROID_DO_NOT_MIX = 1,
   */

  //called at the beginning of the component lifecycle
  //configures the Audio component
  async componentDidMount() {
    await Audio.setAudioModeAsync({
      allowsRecordingIOS: false,
      playThroughEarpieceAndroid: false,
      interruptionModeIOS: INTERRUPTION_MODE_IOS_DO_NOT_MIX = 1,
      playsInSilentModeIOS: true,
      shouldDuckAndroid: true,
      interruptionModeAndroid: INTERRUPTION_MODE_ANDROID_DO_NOT_MIX = 1,
    });
    //loadAudio loads the audio for our player.
    this.loadAudio();
  }

  //checks the values of this.state.isPlaying to determine
  //if the track should be played or paused. Then calls the associated method on the 
  //playbackInstance.
  //then updates the value of isPlaying in the state.
  handlePlayPause = async () => {
    const { isPlaying, playbackInstance } = this.state;
    isPlaying ? await playbackInstance.pauseAsync() : await playbackInstance.playAsync();
    this.setState({
      isPlaying: !isPlaying
    });
  }
  //handles skipping to the previous track
  //Clears the current track from the playbackInstance by calling unloadAsync. 
  //Updates the currentTrackIndex value of state to either one less than current or 0 if at the 
  //beginning. THen calls this.loadAudio to load the new track.
  handlePreviousTrack = async () => {
    let { playbackInstance, currentTrackIndex } = this.state;
    if (playbackInstance) {
      await playbackInstance.unloadAsync();
      currentTrackIndex === 0 ? currentTrackIndex = playlist.length - 1 : currentTrackIndex -= 1;
      this.setState({
        currentTrackIndex
      });
      this.loadAudio();
    }
  }

  //same sort of logic as handlePreviousTrack, but for skipping to the next track
  handleNextTrack = async () => {
    let { playbackInstance, currentTrackIndex } = this.state;
    if (playbackInstance) {
      await playbackInstance.unloadAsync();
      currentTrackIndex < playlist.length - 1 ? currentTrackIndex += 1 : currentTrackIndex = 0;
      this.setState({
        currentTrackIndex
      });
      this.loadAudio();
    }
  }

  //define the callback for handling status updates. 
  //sets the value of isBuffering on state to isBuffereing from status. 
  onPlaybackStatusUpdate = (status) => {
    this.setState({
      isBuffering: status.isBuffering
    });
  }


  /**
   * Loads the Audio for our player. 
   */
  async loadAudio() {
    //create a new Audio instance
    const playbackInstance = new Audio.Sound();
    const source = {
      uri: playlist[this.state.currentTrackIndex].uri
    }
    const status = {
      shouldPlay: this.state.isPlaying,
      volume: this.state.volume,
    };
    //setOnPlaybackStatusUpdate gets a handler that will be caleld when the the state of 
    //the playback has changed. 
    playbackInstance
      .setOnPlaybackStatusUpdate(
        this.onPlaybackStatusUpdate
      );
    //loadAsync gets a source from the playlist array and status object with the 
    //volume and other properties like if we want to wait for it to finish downloading before 
    //starting to play
    await playbackInstance.loadAsync(source, status, false);
    this.setState({
      playbackInstance
    });
  }

  //renderSongInfo returns the JSX for displaying the metadata for the current track
  renderSongInfo() {
    const { playbackInstance, currentTrackIndex } = this.state;
    return playbackInstance ?
      <View style={styles.trackInfo}>
        <Text style={[styles.trackInfoText, styles.largeText]}>
          {playlist[currentTrackIndex].title}
        </Text>
        <Text style={[styles.trackInfoText, styles.smallText]}>
          {playlist[currentTrackIndex].artist}
        </Text>
        <Text style={[styles.trackInfoText, styles.smallText]}>
          {playlist[currentTrackIndex].album}
        </Text>
      </View>
      : null;
  }//ends renderSongInfo

  // ! ////////////////////////////////////////// EXERCISE 4 CODE //////////////////////////////////////////////////////////////
  /*
  componentWillMount is called right after a constructor in a class component.
  it is a lifecycle hook that is a little depricated. 
  We use this to load the existing stored value it it exists.

  */
  /*
   componentWillMount() {
     this.onLoad();
   }
 */
  /*
  onLoad is invoked when loading of the component is successfully completed.
  Loads the current content from the local storage. Simmilary to localStorage in browser,
  you just need to jse the key we defined when saving the data. 
  */
  onLoad = async () => {
    try {
      const ratingsObjString = await AsyncStorage.getItem(key);
      const savedRatings = JSON.parse(ratingsObjString).ratings;
      this.setState({
        storedRatings: savedRatings,
        ratings: savedRatings
      });
    } catch (error) {
      Alert.alert('Error', `There was an error while loading the data: ${error}`);
    }
  }

  onSave = async () => {
    try {
      let ratingsObject = { ratings: this.state.ratings };
      await AsyncStorage.setItem(key, JSON.stringify(ratingsObject));
      Alert.alert('Saved', 'Successfully saved on device');
    } catch (error) {
      Alert.alert('Error', `There was an error while saving the data: ${error}`);
    }
  }

  onChange = (data) => {
    this.setState({ data });
  }

  buildDisplay = () => {
    const { ratings } = this.state;
    let displayString = "";
    for (let i = 0; i < ratings.length; i++) {
      displayString += `Q${i + 1}: ${ratings[i]}\n`
    }

    return displayString;
  }


  /**
   * we need 3 pieces in our UI, a "Buffering..." message to show the user during
   * buffering, a section for displaying info on the current track, and a section
   * to hold the players controls. The message only displays when 
   * both isBuffering and isPlaying from the App state are true. 
   * The song info is rendered via the renderSongInfo method. 
   * 
   * The actual controls are made of 3 buttons, and each button has an associated 
   * icon from the Feather library. 
   */

  render() {
    /*
    We can write regular JavaScript outside of the return but inside of the render and do
    conditional rendering in this way!
    We will set the current picker to be a variable and then render a different picker depending on the song that is currently
    displayed.
    */
    const pickerArray = [
      <Picker
        onValueChange={(itemValue, itemIndex) => {
          const ratingsArray = this.state.ratings;
          ratingsArray[0] = itemValue;
          this.setState({ ratings: ratingsArray });
        }
        }>
        <Picker.Item label="1 Star" value='1 Star' />
        <Picker.Item label="2 Stars" value='2 Stars' />
        <Picker.Item label="3 Stars" value='3 Stars' />
        <Picker.Item label="4 Stars" value='4 Stars' />
        <Picker.Item label="5 Stars" value='5 Stars' />
      </Picker>,
      <Picker
        onValueChange={(itemValue, itemIndex) => {
          const ratingsArray = this.state.ratings;
          ratingsArray[1] = itemValue;
          this.setState({ ratings: ratingsArray });
        }
        }>
        <Picker.Item label="1 Star" value='1 Star' />
        <Picker.Item label="2 Stars" value='2 Stars' />
        <Picker.Item label="3 Stars" value='3 Stars' />
        <Picker.Item label="4 Stars" value='4 Stars' />
        <Picker.Item label="5 Stars" value='5 Stars' />
      </Picker>
      ,
      <Picker
        onValueChange={(itemValue, itemIndex) => {
          const ratingsArray = this.state.ratings;
          ratingsArray[2] = itemValue;
          this.setState({ ratings: ratingsArray });
        }
        }>
        <Picker.Item label="1 Star" value='1 Star' />
        <Picker.Item label="2 Stars" value='2 Stars' />
        <Picker.Item label="3 Stars" value='3 Stars' />
        <Picker.Item label="4 Stars" value='4 Stars' />
        <Picker.Item label="5 Stars" value='5 Stars' />
      </Picker>
    ];

    //picker holds the current picker
    let currentPicker = pickerArray[0];
    let currentText = this.state.questions[0];

    switch (this.state.currentTrackIndex) {
      case 0:
        currentPicker = pickerArray[0];
        currentText = this.state.questions[0];
        break;
      case 1:
        currentPicker = pickerArray[1];
        currentText = this.state.questions[1];
        break;
      case 2:
        currentPicker = pickerArray[2];
        currentText = this.state.questions[2];
        break;
      default:
        currentPicker = pickerArray[0];
        currentText = this.state.questions[0];
    }//ends switch

    return (
      <SafeAreaView style={styles.container}>
        <ScrollView>

          <View style={styles.container}>
            <Text style={[styles.largeText, styles.buffer]}>
              {this.state.isBuffering && this.state.isPlaying ? 'Buffering...' : null}
            </Text>
            {this.renderSongInfo()}
            <View style={styles.controls}>
              <TouchableOpacity
                style={styles.control}
                onPress={this.handlePreviousTrack}
              >
                <Feather name="skip-back" size={32} color="#fff" />
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.control}
                onPress={this.handlePlayPause}
              >
                {this.state.isPlaying ?
                  <Feather name="pause" size={32} color="#fff" /> :
                  <Feather name="play" size={32} color="#fff" />
                }
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.control}
                onPress={this.handleNextTrack}
              >
                <Feather name="skip-forward" size={32} color="#fff" />
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.group}>
            <Text> {currentText} </Text>
            {currentPicker}
          </View>

          <Text style={styles.preview}>{this.buildDisplay()}</Text>
          <TouchableOpacity onPress={this.onSave} style={styles.button}>
            <Text>Save locally</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.onLoad} style={styles.button}>
            <Text>Load data</Text>
          </TouchableOpacity>


        </ScrollView>
      </SafeAreaView >
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#CAD6D6',
    alignItems: 'center',
    justifyContent: 'center',
  },
  trackInfo: {
    padding: 40,
    backgroundColor: '#191A1A',
  },
  buffer: {
    color: '#fff'
  },
  trackInfoText: {
    textAlign: 'center',
    flexWrap: 'wrap',
    color: '#fff'
  },
  largeText: {
    fontSize: 22
  },
  smallText: {
    fontSize: 16
  },
  control: {
    margin: 20
  },
  controls: {
    flexDirection: 'row'
  },
  preview: {
    backgroundColor: '#bdc3c7',
    width: 300,
    height: 100,
    padding: 10,
    borderRadius: 5,
    color: '#333',
    marginBottom: 1,
  },
  input: {
    backgroundColor: '#ecf0f1',
    borderRadius: 3,
    width: 300,
    height: 40,
    padding: 5,
  },
  button: {
    backgroundColor: '#f39c12',
    padding: 1,
    borderRadius: 3,
    marginTop: 10,
  },
  group: {
    flex: 'row',
  }

});
