## notes
The app knows how to load an audio file from the playlist array and update the 
state of the app with the current buffering status of the loaded audio file. 
We use this later in render to display a message to the user. 

upon successfuly loading the app, it should look like the following:
![](./assets/runningApp.png)